package com.example.juego_examen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var startGame:Button
    private lateinit var tapMeButton: Button

    private lateinit var countDownTimer: CountDownTimer
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000

    private  var timeLeft = 10
    private var gameScore =0
    private var isGameStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //gameScoreTextView = findViewById(R.id.)
        tapMeButton = findViewById(R.id.id_button_tapme)


        if(savedInstanceState != null){
            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()

        }

        resetGame()
    }
    override fun onDestroy() {
        super.onDestroy()
        countDownTimer.cancel()

    }

    private fun incrementScore(){
        if(!isGameStarted){
            startGame()
        }
        gameScore ++
        gameScoreTextView.text = getString(R.string.score, gameScore)
    }

    private fun resetGame() {
        gameScore = 0

        timeLeft = 10

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){ // object es una clase generica concreta que hereda de  la calse abstracta countDownTimer

            override fun onFinish() { // se ejecuta cuando el contadore llega a cero
                endGame()
            }

            override fun onTick(millisUnitlFinished: Long) { // se ejecuta cada segundo
                timeLeft = millisUnitlFinished.toInt()/1000
            }


        }
        isGameStarted = false
    }

    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
    }
    private fun endGame(){
        resetGame()
    }
    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score, gameScore)


        configCountDownTimer()

        if (isGameStarted){
            countDownTimer.start()

        }
    }

    private fun configCountDownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval){
            override fun onTick(millisUnitFinished: Long) {
                timeLeft = millisUnitFinished.toInt()/1000

            }

            override fun onFinish() {
                endGame()
            }

        }

    }

    companion object{ // se crear porq ue no existe el static y sirve para almacenar el valor de las variables
        private const val  SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"



    }
}
